NUMBER_OF_LANES = 15;
NUMBER_OF_TIMES = 28;

// Add headers L1, L2
for (i = 0; i < NUMBER_OF_LANES; ) {
  $(".time-table thead tr").append(`<th>L${++i}</th>`);
}

// Add body
hrCount = -1;
packNumbers = {
  1: [14],
  4: [0],
};
skipNumbers = {
  2: [14],
  3: [14],
  4: [14],
  5: [0],
  6: [0],
  7: [0],
};
for (i = 0; i < NUMBER_OF_TIMES; i++) {
  hrCount += i % 4 == 0 ? 1 : 0;
  let htmlTemplate = "";
  htmlTemplate += `
    <tr>
      <td >
        <div>${9 + hrCount}:${(i % 4) * 15} AM</div>
      </td>
  `;
  for (j = 0; j < NUMBER_OF_LANES; j++) {
    if (!(i in skipNumbers && skipNumbers[i].indexOf(j) > -1)) {
      if (packNumbers[i] && packNumbers[i].indexOf(j) > -1) {
        htmlTemplate += `
        <td rowspan="4">
          <div  class="border border-danger bg-primary"><i class="fa fa-solid fa-eye" data-laneNumber="${
            i + 1
          }"  
          data-time="${9 + hrCount}:${(i % 4) * 15}"></i></div>
        </td>
      `;
      } else {
        htmlTemplate += `
        <td rowspan="1">
          <div  class=" "><i class="fa fa-solid fa-plus"></i></div>
        </td>
      `;
      }
    }
  }
  htmlTemplate += "</tr>";
  $(".time-table tbody").append(htmlTemplate);
}

$(".time-table tr td .fa-plus").on("click", function (e) {
  $(".initial-text").show();
  $(".booking-info").hide();
  alert("Perform add action");
});
$(".time-table tr td .fa-eye").on("click", function () {
  $(".initial-text").hide();
  $(".booking-info span").text($(this).attr("data-time"));
  $(".booking-info").show();
});
